import React, { useState } from 'react'
import CartShoe from './CartShoe'
import { data_shoe } from './data_shoe'
import ListShoe from './ListShoe'

function Ex_shoe_shop() {
    const [listShoe] = useState(data_shoe);
    const [cartShoe, setCartShoe] = useState([]);

    let themGioHang = (shoe) => {
        let cloneCart = [...cartShoe];
        let index = cloneCart.findIndex((item) => {
            return item.id == shoe.id
        });
        if (index == -1) {
            let newCart = { ...shoe, soLuong: 1 };
            cloneCart.push(newCart);
        } else {
            cloneCart[index].soLuong += 1;
        }
        setCartShoe(cloneCart);
    }

    let xoaGioHang = (id) => {
        let cloneCart = [...cartShoe];
        let index = cloneCart.filter(item => {
            return item.id != id
        })
        setCartShoe(index);
    }

    let tangGiam = (id, luachon) => {
        let cloneCart = [...cartShoe];
        let index = cloneCart.findIndex((item) => {
            return item.id == id
        })
        cloneCart[index].soLuong += luachon;
        if (cloneCart[index].soLuong == 0) {
            cloneCart.splice(index, 1)
        }
        setCartShoe(cloneCart);
    }




    return (
        <div className=' text-center'>
            <h2>Ex_shoe_shop</h2>
            <div className='row'>
                <div className='col-7'>
                    <CartShoe cart={cartShoe} xoaGioHang={xoaGioHang} tangGiam={tangGiam} />
                </div>
                <div className='col-5'>
                    <ListShoe listShoe={listShoe} themGioHang={themGioHang} />
                </div>
            </div>


        </div>
    )
}
export default Ex_shoe_shop