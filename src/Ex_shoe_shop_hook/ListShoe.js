import React from 'react'
import ItemShoe from './ItemShoe'

function ListShoe({ listShoe, themGioHang }) {
    return (
        <div className='container '>

            <div className='row'>
                {listShoe.map((item, index) => {
                    return <ItemShoe shoe={item} key={index} themGioHang={themGioHang} />;
                })}
            </div>
        </div>
    )
}
export default ListShoe