import React from 'react'

function ItemShoe({ shoe, themGioHang }) {
    return (

        <div className='col-4 mb-5' style={{ border: "1px solid black", borderRadius: "10px", padding: "10px" }}>
            <img style={{ width: "50%" }} src={shoe.image} alt="" />
            <div >
                <h5>{shoe.name}</h5>
                <p>{shoe.price}</p>
                <a onClick={() => { themGioHang(shoe) }} className='btn btn-danger' href="#">Add</a>
            </div>
        </div>
    )
}
export default ItemShoe