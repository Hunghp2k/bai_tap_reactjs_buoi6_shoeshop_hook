import React from 'react'

function CartShoe({ cart, xoaGioHang, tangGiam }) {
    let rendertbody = () => {
        return cart.map((item, index) => {
            return (
                <tr key={index}>
                    <th>{item.id}</th>
                    <th>{item.name}</th>
                    <th>{item.price}</th>
                    <th>
                        <button onClick={() => { tangGiam(item.id, -1) }} className='btn btn-primary'>-</button>
                        <strong>{item.soLuong}</strong>
                        <button onClick={() => { tangGiam(item.id, +1) }} className='btn btn-success'>+</button>
                    </th>
                    <th>
                        <img style={{ width: 50 }} src={item.image} alt="" />
                    </th>
                    <th>{item.soLuong * item.price}</th>
                    <th>
                        <button onClick={() => { xoaGioHang(item.id) }} className='btn btn-danger'>Xóa</button>
                    </th>
                </tr>
            )
        })
    }
    return (
        <div>
            <h2>CartShoe</h2>
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Img</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {rendertbody()}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
export default CartShoe